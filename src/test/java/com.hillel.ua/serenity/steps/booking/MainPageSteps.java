package com.hillel.ua.serenity.steps.booking;

import com.hillel.ua.core.pages.booking.BookingMainPage;
import com.hillel.ua.core.panels.booking.CalendarPanel;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.text.ParseException;

public class MainPageSteps extends ScenarioSteps {

    private BookingMainPage bookingMainPage;

    public MainPageSteps(final Pages pages) {
        this.bookingMainPage = pages.getPage(BookingMainPage.class);
    }

    @Step
    public void setCheInCheckOutDate(final String cheInCheckOutDate) throws ParseException {
        final CalendarPanel calendarPanel = bookingMainPage.openCalendar();
        calendarPanel.setCheckInCheckOutDate(cheInCheckOutDate);
    }
}
