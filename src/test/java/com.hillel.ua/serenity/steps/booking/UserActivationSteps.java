package com.hillel.ua.serenity.steps.booking;

import com.hillel.ua.core.dto.booking.MailConfigurationDTO;
import com.hillel.ua.core.helper.MailHelper;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import javax.mail.MessagingException;
import java.io.IOException;

public class UserActivationSteps extends ScenarioSteps {

    @Step
    public String getMailBodyHtmlWithActivationLink(final MailConfigurationDTO mailConfigurationDTO) {
        try {
            return MailHelper.getMessageContent(mailConfigurationDTO);
        } catch (final IOException | MessagingException e) {
            throw new IllegalStateException("Unable to get the mail content by subject!", e);
        }
    }
}
