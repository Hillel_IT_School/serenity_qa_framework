package com.hillel.ua.serenity.steps.booking;

import com.hillel.ua.core.pages.booking.RegistrationPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class UserRegistrationSteps extends ScenarioSteps {

    private RegistrationPage registrationPage;

    public UserRegistrationSteps(final Pages pages) {
        super();
        this.registrationPage = pages.getPage(RegistrationPage.class);
    }

    @Step
    public void typeMailAddress(final String mailAddress) {
        registrationPage.getRegistrationPanel().
                setMail(mailAddress);
    }

    @Step
    public void typePassword(final String password) {
        registrationPage.getRegistrationPanel()
                .setPassword(password);
    }

    @Step
    public void typeConfirmedPassword(final String confirmedPassword) {
        registrationPage.getRegistrationPanel()
                .setConfirmedPassword(confirmedPassword);
    }

    @Step
    public void clickCreateAccountButton() {
        registrationPage.getRegistrationPanel().clickCreateAccountButton();
    }

    @Step
    public void clickGetStartedButton() {
        registrationPage.getRegistrationPanel().clickGetStartedButton();
    }

    @Step
    public void openPage(final String pageLink) {
        registrationPage.openPageByLink(pageLink);
    }

    public void clickNextButton() {
    }
}
