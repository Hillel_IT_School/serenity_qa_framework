package com.hillel.ua.serenity.steps.sportcheck;

import com.hillel.ua.core.pages.sportcheck.SportcheckMainPage;
import com.hillel.ua.core.panels.sportcheck.MenuBarPanel;
import com.hillel.ua.core.panels.sportcheck.SideBarNavigationPanel;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;

public class SideBarPanelSteps extends ScenarioSteps {

    private SportcheckMainPage mainPage;

    public SideBarPanelSteps(final Pages pages) {
        this.mainPage = pages.getPage(SportcheckMainPage.class);
    }

    @Step
    public List<String> getSideBarPanelItems() {
        final MenuBarPanel menuBarPanel = mainPage.getMenuBarPanel();
        final SideBarNavigationPanel sideBarNavigationPanel = menuBarPanel.getSideBarNavigationPanel();
        return sideBarNavigationPanel.getSideBarNavigationItemsJava8();
    }

    @Step
    public void openPage(final String link) {
        mainPage.openPageByLink(link);
    }
}
