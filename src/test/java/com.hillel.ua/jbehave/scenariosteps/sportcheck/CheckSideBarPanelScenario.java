package com.hillel.ua.jbehave.scenariosteps.sportcheck;

import com.hillel.ua.serenity.steps.sportcheck.SideBarPanelSteps;
import com.hillel.ua.utils.PropertiesUtils;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.util.List;

public class CheckSideBarPanelScenario {

    @Steps
    private SideBarPanelSteps panelSteps;

    @Given("user opened following page '$link'")
    public void userOpenedPage(final String link) {
        final String siteLink = PropertiesUtils.readProperty("sportchek.main.url");
        panelSteps.openPage(link);
    }

    @When("user clicks 'Shop Categories' button")
    public void userClicksShopCategoriesButton() {
        final List<String> actualItems = panelSteps.getSideBarPanelItems();
        Serenity.setSessionVariable("sideBarPanelItems").to(actualItems);
    }

    @Then("following items should be displayed '$items'")
    public void checkSideBarItemsDisplayed(final List<String> expectedItems) {
        final List<String> actualItems = Serenity.sessionVariableCalled("sideBarPanelItems");
        Assert.assertArrayEquals("There are incorrect items displayed!",
                expectedItems.toArray(), actualItems.toArray());
    }
}
