package com.hillel.ua.jbehave.scenariosteps.booking;

import com.hillel.ua.core.dto.booking.MailConfigurationDTO;
import com.hillel.ua.serenity.steps.booking.UserActivationSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class UserActivationScenario {

    @Steps
    private UserActivationSteps activationSteps;

    @When("user activates created account by email, using next configuration: $configuration")
    public void activateUserAccount(final ExamplesTable examplesTable) {
        final String activationLinkCss = "a[class*='link']";
        final MailConfigurationDTO configuration = examplesTable.getRowsAs(MailConfigurationDTO.class).get(0);
        final String html = activationSteps.getMailBodyHtmlWithActivationLink(configuration);
        final Document doc = Jsoup.parse(html);
        final String link = doc.select(activationLinkCss).attr("href");
    }
}
