package com.hillel.ua.jbehave.scenariosteps.booking;

import com.hillel.ua.core.dto.booking.UserRegistrationCredentialsDTO;
import com.hillel.ua.serenity.steps.booking.UserActivationSteps;
import com.hillel.ua.serenity.steps.booking.UserRegistrationSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import java.util.List;

public class UserRegistrationScenario {

    @Steps
    private UserRegistrationSteps registrationSteps;

    @Given("user opens 'Booking Registration page' by following link: '$link'")
    @Alias("user opens 'Booking Main' page: '$link'")
    public void openRegistrationPage(final String registrationPageLink) {

    }

    @When("user logins to site, using next credentials: $credentials")
    public void typeLoginUserLoginCredentials(final ExamplesTable credentials) {
        final UserRegistrationCredentialsDTO credentialsDTO = credentials.getRowsAs(UserRegistrationCredentialsDTO.class).get(0);
        registrationSteps.typeMailAddress(credentialsDTO.getEmail());
        registrationSteps.clickNextButton();
    }

    @Then("'$user' user should be logged in")
    public void isCreatedUserActivatedAndLoggedIn(final String userName) {

    }

    @Then("user profile's menu should contain following items: '$items'")
    public void isUserMenuContainItems(final List<String> menuItems) {

    }
}
