package com.hillel.ua.jbehave.scenariosteps.booking;

import com.hillel.ua.core.dto.booking.TravelingInfoDTO;
import com.hillel.ua.serenity.steps.booking.MainPageSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import java.text.ParseException;

public class BookingMainPageScenario {

    @Steps
    private MainPageSteps pageSteps;

    @When("user selects next traveling filters: $travelInfo")
    public void setTravelingInfo(final ExamplesTable travelingInfo) throws ParseException {
        final TravelingInfoDTO travelInfo = travelingInfo.getRowsAs(TravelingInfoDTO.class).get(0);
        final String[] checkInCheckOutDates = travelInfo.getCheckInCheckOutDate().split("-");
        pageSteps.setCheInCheckOutDate(checkInCheckOutDates[0]);
        pageSteps.setCheInCheckOutDate(checkInCheckOutDates[1]);
    }

    @When("user clicks 'Search' button")
    public void clickSearchButton() {

    }

    @Then("first hotel from the search result list should placed in '$destination'")
    public void isTheFirstHotelFromTheListPlacedIn(final String expectedCity) {

    }
}
