package com.hillel.ua.jbehave;

import net.serenitybdd.jbehave.SerenityStories;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.steps.ParameterControls;

import java.util.Optional;

public class AcceptanceTestSuite extends SerenityStories {

    public AcceptanceTestSuite() {
        super();
        final String storyNameAsParameter = System.getProperty("story.name");
        Optional.ofNullable(storyNameAsParameter).ifPresent(this::findStoriesCalled);
        configuration().useParameterControls(new ParameterControls());
        configuration().usePendingStepStrategy(new FailingUponPendingStep());
    }
}
