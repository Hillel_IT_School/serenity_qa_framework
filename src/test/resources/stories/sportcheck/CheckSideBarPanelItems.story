Narrative:
In order to check sidebar navigation functionality
As a user, I want to see displayed items

Scenario: User able to see displayed items on sidebar navigation panel

Given user opened following page '<link>'
When user clicks 'Shop Categories' button
Then following items should be displayed 'Deals & Features, Men, Women, Kids, Shoes & Footwear, Gear, Electronics, Jerseys & Fan Wear, Sneaker Launches, Shop by Brand, Chek advice'

Examples:
|link|
|https://www.sportchek.ca/|
|https://google.com/|

