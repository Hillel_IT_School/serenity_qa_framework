Meta:

Narrative:
As a user
I want to register and activate my account
So that I can achieve a business goal

Scenario: Check user able to receive email notification in case of registration

Given user opens 'Booking Main' page: 'https://account.booking.com/sign-in'
When user logins to site, using next credentials:
| email                 | password    | confirmPassword |
| hillel.user@gmail.com | Hillel@2018 | Hillel@2018     |
And user activates created account by email
Then 'hillel user' user should be logged in
And user profile's menu should contain following items: 'My dashboard, Bookings, Reviews, My lists, Get the app, Customer service help, Settings'