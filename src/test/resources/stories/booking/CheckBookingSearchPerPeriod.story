Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Given user opens 'Booking Main' page: 'https://www.booking.com'
When user selects next traveling filters:
| destination   | checkInCheckOutDate            | roomsCount | adultsCount | childrenCount |
| New York City | 9 January 2019 - 17 March 2019 | 1          | 1           | 1             |
And user clicks 'Search' button
Then first hotel from the search result list should placed in 'New York City'