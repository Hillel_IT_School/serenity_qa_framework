package com.hillel.ua.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {

    private final Properties properties = new Properties();
    private static final String propertyFileName = System.getProperty("properties.file");
    private static PropertiesUtils propertiesUtils;

    public static String readProperty(final String propertyKey) {
        return newInstance().properties.getProperty(propertyKey);
    }

    private PropertiesUtils() throws IOException {
        newInstance();
        final InputStream inputStream = this.getClass().getResourceAsStream(propertyFileName);
        properties.load(inputStream);
    }

    private static PropertiesUtils newInstance() {
        if (propertiesUtils == null) {
            try {
                propertiesUtils = new PropertiesUtils();
            } catch (final IOException e) {
                throw new IllegalStateException(String.format("Missing [%s] property file", propertyFileName));
            }
        }
        return propertiesUtils;
    }
}
