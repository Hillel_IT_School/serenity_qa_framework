package com.hillel.ua.core.dto.booking;

import lombok.Data;

@Data
public class TravelingInfoDTO {

    private String destination;
    private String checkInCheckOutDate;
    private Integer roomsCount;
    private Integer adultsCount;
    private Integer childrenCount;
}
