package com.hillel.ua.core.dto.booking;

import lombok.Data;

@Data
public class UserRegistrationCredentialsDTO {

    private String email;
    private String password;
    private String confirmPassword;
}
