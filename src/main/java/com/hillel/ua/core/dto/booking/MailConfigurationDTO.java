package com.hillel.ua.core.dto.booking;

import lombok.Data;

@Data
public class MailConfigurationDTO {

    private String server;
    private String userName;
    private String password;
    private String subject;
}
