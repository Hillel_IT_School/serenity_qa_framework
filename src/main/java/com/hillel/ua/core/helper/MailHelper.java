package com.hillel.ua.core.helper;

import com.hillel.ua.core.dto.booking.MailConfigurationDTO;

import javax.mail.*;
import java.io.IOException;
import java.util.Properties;

public class MailHelper {

    private static boolean textIsHtml = false;

    private MailHelper() {
    }

    private static Folder initMailActions(final MailConfigurationDTO mailConfigurationDTO) throws MessagingException {
        final Properties properties = new Properties();
        properties.put("mail.pop3.host", mailConfigurationDTO.getServer());
        properties.put("mail.pop3.port", "995");
        properties.put("mail.pop3.starttls.enable", "true");
        final Session emailSession = Session.getDefaultInstance(properties);
        final Store store = emailSession.getStore("pop3s");
        store.connect(mailConfigurationDTO.getServer(), mailConfigurationDTO.getUserName(), mailConfigurationDTO.getPassword());
        final Folder folder = store.getFolder("INBOX");
        folder.open(Folder.READ_ONLY);
        store.close();
        return folder;
    }

    public static String getMessageContent(final MailConfigurationDTO mailConfigurationDTO) throws MessagingException, IOException {
        final Folder inboxFolder = initMailActions(mailConfigurationDTO);
        if (!inboxFolder.isOpen()) {
            inboxFolder.open(Folder.READ_ONLY);
        }
        final Message[] messages = inboxFolder.getMessages();
        for (final Message message : messages) {
            if (message.getSubject().equalsIgnoreCase(mailConfigurationDTO.getSubject())) {
                return getText(message);
            }
        }
        return null;
    }

    private static String getText(final Part p) throws MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            final String s = (String) p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }
        if (p.isMimeType("multipart/alternative")) {
            final Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                final Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null) {
                        text = getText(bp);
                    }
                } else if (bp.isMimeType("text/html")) {
                    final String s = getText(bp);
                    if (s != null) {
                        return s;
                    }
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            final Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                final String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }
        return null;
    }
}
