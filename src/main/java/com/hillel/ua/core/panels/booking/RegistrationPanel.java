package com.hillel.ua.core.panels.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.AbstractPanel;
import net.serenitybdd.core.pages.WebElementFacade;

public class RegistrationPanel extends AbstractPanel {

    private static final String MAIL_INPUT = ".//input[@id='login_name_register']";
    private static final String CREATE_PASSWORD_INPUT = ".//input[@id='password']";
    private static final String CONFIRMED_PASSWORD_INPUT = ".//input[@id='confirmed_password']";
    private static final String CREATE_ACCOUNT_BUTTON = ".//button[@type='submit']";

    public RegistrationPanel(WebElementFacade panelBaseLocation, AbstractPage rootPage) {
        super(panelBaseLocation, rootPage);
    }

    public void setMail(final String userMail) {
        getRootPage().findBy(MAIL_INPUT)
                .then()
                .type(userMail);
    }

    public void clickGetStartedButton() {
        getRootPage().findBy(CREATE_ACCOUNT_BUTTON)
                .then()
                .click();
    }

    public void clickCreateAccountButton() {
        clickGetStartedButton();
    }

    public void setPassword(final String password) {
        getRootPage().findBy(CREATE_PASSWORD_INPUT)
                .then()
                .type(password);
    }

    public void setConfirmedPassword(final String confirmedPassword) {
        getRootPage().findBy(CONFIRMED_PASSWORD_INPUT)
                .then()
                .type(confirmedPassword);
    }
}
