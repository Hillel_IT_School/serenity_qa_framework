package com.hillel.ua.core.panels.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.AbstractPanel;
import net.serenitybdd.core.pages.WebElementFacade;
import org.joda.time.DateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CalendarPanel extends AbstractPanel {

    private static final String MOVE_FORWARD_BUTTON = ".(//div[@class='c2-button c2-button-earlier'])[1]";
    private static final String MOVE_NEXT_BUTTON = ".(//div[@class='c2-button c2-button-further'])[1]";
    private static final String CALENDAR_ITEMS = ".//table[@class='c2-month-table']";

    public CalendarPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        super(panelBaseLocation, rootPage);
    }

    public void setCheckInCheckOutDate(final String travelingDate) throws ParseException {
        final SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM, yyyy", Locale.US); //June 27,  2007
        final Date travelDate = fmt.parse(travelingDate);
        final DateTime dateTime = new DateTime(travelDate);
        final List<WebElementFacade> calendarItems = getRootPage().findAll(CALENDAR_ITEMS);//32
        for (final WebElementFacade calendarItem : calendarItems) {
            final WebElementFacade month = calendarItem.findBy("./th");
            if (!month.getText().contains(dateTime.toString("MMMM"))) {
                final WebElementFacade moveNextButton = getRootPage().findBy(MOVE_FORWARD_BUTTON);
                moveNextButton.click();
            } else {
                final List<WebElement> days = calendarItem.findElements(By.xpath("./tbody//td"));
                setDay(days, dateTime.toString("dd"));
            }
        }
    }

    private void setDay(final List<WebElement> days,final String requiredDay) {
        for (final WebElement dayItem : days) {
            final String day = dayItem.getText();
            if (day.equals(requiredDay)) {
                dayItem.click();
            }
        }
    }
}
