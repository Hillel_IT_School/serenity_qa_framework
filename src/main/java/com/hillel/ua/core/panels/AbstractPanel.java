package com.hillel.ua.core.panels;

import com.hillel.ua.core.WebDriverAdaptor;
import com.hillel.ua.core.pages.AbstractPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.locators.SmartElementLocatorFactory;
import net.thucydides.core.annotations.locators.SmartFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public abstract class AbstractPanel {

    private long waitForTimeoutInMilliseconds = 5000;
    private AbstractPage rootPage;
    private WebDriverAdaptor panelToWebDriver;

    public AbstractPage getRootPage() {
        return rootPage;
    }

    protected AbstractPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        this.rootPage = rootPage;
        initPanel(panelBaseLocation, rootPage);
    }

    private void initPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        this.rootPage = rootPage;
        waitForTimeoutInMilliseconds = rootPage.waitForTimeoutInMilliseconds();
        panelToWebDriver = new WebDriverAdaptor(panelBaseLocation, getDriver());
        final ElementLocatorFactory finder = new SmartElementLocatorFactory(panelToWebDriver, null,
                (int) waitForTimeoutInSeconds());
        final FieldDecorator decorator = new SmartFieldDecorator(finder, getDriver(), rootPage);
        PageFactory.initElements(decorator, this);
    }

    private long waitForTimeoutInSeconds() {
        return (waitForTimeoutInMilliseconds < 1000) ? 1 : (waitForTimeoutInMilliseconds / 1000);
    }
}
