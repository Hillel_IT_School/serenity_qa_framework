package com.hillel.ua.core.panels.sportcheck;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.AbstractPanel;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SideBarNavigationPanel extends AbstractPanel {

    private static final String SIDEBAR_NAV_MENU_ITEMS = ".//ul[contains(@class, 'page-nav__list page-nav__list_short')]/li[@class='page-nav__item']";

    protected SideBarNavigationPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        super(panelBaseLocation, rootPage);
    }

    public List<String> getSideBarNavigationItemsJava8() {
        return getRootPage().findAll(By.xpath(SIDEBAR_NAV_MENU_ITEMS))
                .parallelStream()
                .map(item -> item.getText())
                .collect(Collectors.toList());
    }

    public List<String> getSideBarNavigationItems() {
        final List<String> sideBarItemsText = new ArrayList<>();
        final List<WebElementFacade> sideBarItems = new ArrayList<>();
        for (final WebElementFacade item : sideBarItems) {
            sideBarItemsText.add(item.getText());
        }
        return sideBarItemsText;
    }
}
