package com.hillel.ua.core.panels.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.AbstractPanel;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.ArrayList;
import java.util.List;

public class HeaderPanel extends AbstractPanel {

    private static final String HEADER_FIRST_NAME = ".//span[contains(@class, 'user_firstname')]";
    private static final String HEADER_LAST_NAME = ".//span[contains(@class, 'user_lastname')]";
    private static final String USER_AVATAR = ".//span[contains(@class, 'user_avatar')]";
    private static final String LOGGED_USER_MENU_ITEMS = ".//a[contains(@class, 'profile-menu__link')]";

    public HeaderPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        super(panelBaseLocation, rootPage);
    }

    public String getLoggedInUserName() {
        final String firstName = getRootPage().findBy(HEADER_FIRST_NAME).then().getText();
        final String lastName = getRootPage().findBy(HEADER_LAST_NAME).then().getText();
        return String.format("%s %s", firstName, lastName);
    }

    public List<String> getLoggedUserMenuItems() {
        final List<String> menuItems = new ArrayList<>();
        final List<WebElementFacade> menuElements = getRootPage().findAll(LOGGED_USER_MENU_ITEMS);
        menuElements.forEach(menuItem -> {
            menuItems.add(menuItem.getText());
        });
        return menuItems;
    }
}
