package com.hillel.ua.core.panels.sportcheck;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.AbstractPanel;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class MenuBarPanel extends AbstractPanel {

    private static final String HAMBURGER_MENU_BUTTON = ".//div[@data-menu-toggle]";
    private static final String SIDEBAR_NAV_PANEL = ".//nav[contains(@class, 'page-nav')]";

    public MenuBarPanel(final WebElementFacade panelBaseLocation, final AbstractPage rootPage) {
        super(panelBaseLocation, rootPage);
    }

    public SideBarNavigationPanel getSideBarNavigationPanel() {
        getRootPage().findBy(HAMBURGER_MENU_BUTTON).then().click();
        return new SideBarNavigationPanel(getRootPage().find(By.xpath(SIDEBAR_NAV_PANEL)), getRootPage());
    }
}
