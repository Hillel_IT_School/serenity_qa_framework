package com.hillel.ua.core.pages.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.booking.HeaderPanel;
import org.openqa.selenium.WebDriver;

public class MainPage extends AbstractPage {

    private static final String HEADER_PANEL = "//div[@class='header-wrapper']";

    public MainPage(final WebDriver driver) {
        super(driver);
    }

    public HeaderPanel getHeaderPanel() {
        return new HeaderPanel(findBy(HEADER_PANEL), this);
    }
}
