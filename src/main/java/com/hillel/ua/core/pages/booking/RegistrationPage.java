package com.hillel.ua.core.pages.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.booking.RegistrationPanel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage extends AbstractPage {

    private static final String REGISTRATION_PANEL = "//div[@class='bui-panel-header']";

    public RegistrationPage(WebDriver driver) {
        super(driver);
    }

    public RegistrationPanel getRegistrationPanel() {
        return new RegistrationPanel(find(By.xpath(REGISTRATION_PANEL)), this);
    }


}
