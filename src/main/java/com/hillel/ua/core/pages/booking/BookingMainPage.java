package com.hillel.ua.core.pages.booking;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.booking.CalendarPanel;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.WebDriver;

public class BookingMainPage extends AbstractPage {

    private static final String DESTINATION_CITY = "//input[@type='search']";
    private static final String OPEN_CALENDAR_BUTTON = "//div[@class='xp__dates-inner']";
    private static final String OPEN_ADDITIONAL_INFO_MODAL_BUTTON = "//div[@data-component='search/group/group-with-modal']";
    private static final String CALENDAR_PANEL = "//div[@class='c2-calendar']";

    public BookingMainPage(final WebDriver driver) {
        super(driver);
    }

    public CalendarPanel openCalendar() {
        findBy(OPEN_CALENDAR_BUTTON).then().click();
        return new CalendarPanel(find(By.xpath(CALENDAR_PANEL)),this);
    }
}
