package com.hillel.ua.core.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public abstract class AbstractPage extends PageObject {

    public AbstractPage(final WebDriver driver) {
        super(driver);
        getDriver().manage().window().maximize();
        setImplicitTimeout(20, ChronoUnit.SECONDS);
    }

    public void openPageByLink(final String link) {
        openAt(link);
    }
}
