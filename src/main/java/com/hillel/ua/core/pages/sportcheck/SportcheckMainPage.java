package com.hillel.ua.core.pages.sportcheck;

import com.hillel.ua.core.pages.AbstractPage;
import com.hillel.ua.core.panels.sportcheck.MenuBarPanel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SportcheckMainPage extends AbstractPage {

    private static final String MENU_BAR_PANEL = "//div[contains(@class, 'page-header__content')]";

    public SportcheckMainPage(final WebDriver driver) {
        super(driver);
    }

    public MenuBarPanel getMenuBarPanel() {
        return new MenuBarPanel(find(By.xpath(MENU_BAR_PANEL)), this);
    }
}
